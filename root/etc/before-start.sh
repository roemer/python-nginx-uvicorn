#!/usr/bin/env bash

# exit on errors
set -e

# place additional startup logic here
echo "[/etc/before-start.sh]: additional startup logic"