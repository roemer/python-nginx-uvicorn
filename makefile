IMAGE_NAME = python-uvicorn

docker: # dist
	docker build -t $(IMAGE_NAME) .

docker-odbc: # dist
	docker build -t $(IMAGE_NAME) -f dockerfile-odbc .

run:
	docker run -p 80:80 $(IMAGE_NAME)

it:
	docker run -it $(IMAGE_NAME) /bin/bash
