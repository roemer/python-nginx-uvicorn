ARG BASE_IMAGE=python:3.7-slim-bullseye

FROM $BASE_IMAGE

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        supervisor \
        curl \
        gettext-base \
        nginx \
        && \
    # clear up apt
    apt-get autoremove --purge -y && apt-get clean && rm -rf /var/lib/apt/lists/*

# copy the file system
COPY ./root/ /

# Set owner of /app to www-data
RUN chown -hR www-data:www-data /app
RUN chmod -R 555 /app

WORKDIR /app

ENV PYTHONPATH=/app/ PORT=80

# nr of workers
ENV WEB_CONCURRENCY=4

EXPOSE 80

CMD ["/bin/bash", "/etc/entrypoint.sh"]
