# Python 3.7/3.8/3.9/3.10 + Supervisor + nginx + uvicorn  

![build status](https://gitlab.com/roemer/python-nginx-uvicorn/badges/master/pipeline.svg)

This is a standard Docker image for hosting Python apps using Supervisord, nginx and Uvicorn (using a gunicorn host). It contains a _very_ basic example app. 

The basis for this image are the standard `python:3.x-slim` images, itself based on Debian 11 'Bullseye'. 

The following images are available:

| Tags | Id | Description |
| ---- | -- | ----------- |
| `3.7` | `registry.gitlab.com/roemer/python-nginx-uvicorn:3.7` | The latest [Python 3.7 image](https://hub.docker.com/_/python) |
| `3.8` | `registry.gitlab.com/roemer/python-nginx-uvicorn:3.8` | The latest [Python 3.8 image](https://hub.docker.com/_/python) |
| `3.9` | `registry.gitlab.com/roemer/python-nginx-uvicorn:3.9` | The latest [Python 3.9 image](https://hub.docker.com/_/python) |
| `3.10` | `registry.gitlab.com/roemer/python-nginx-uvicorn:3.10` | The latest [Python 3.10 image](https://hub.docker.com/_/python) |
| `3.7-odbc` | `registry.gitlab.com/roemer/python-nginx-uvicorn:3.7-odbc` |  The latest [Python 3.7 image](https://hub.docker.com/_/python), extended with the [SQL Server ODBC driver](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15) |
| `3.8-odbc` | `registry.gitlab.com/roemer/python-nginx-uvicorn:3.8-odbc` |  The latest [Python 3.8 image](https://hub.docker.com/_/python), extended with the [SQL Server ODBC driver](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15) |
| `3.9-odbc` | `registry.gitlab.com/roemer/python-nginx-uvicorn:3.9-odbc` |  The latest [Python 3.9 image](https://hub.docker.com/_/python), extended with the [SQL Server ODBC driver](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15) |
| `3.10-odbc` | `registry.gitlab.com/roemer/python-nginx-uvicorn:3.10-odbc` |  The latest [Python 3.10 image](https://hub.docker.com/_/python), extended with the [SQL Server ODBC driver](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver15) |

The images can be found on `registry.gitlab.com/roemer/python-nginx-uvicorn`; see https://gitlab.com/roemer/python-nginx-uvicorn/container_registry

The base image size is around 100 MiB. 

## Getting started

1. Create a `Dockerfile` with this image as a starting point:
    
    ```dockerfile
    FROM registry.gitlab.com/roemer/python-nginx-uvicorn:3.7
    
    # Copy your back-end Python code in
    COPY ./my-app/ /app/
   
    # Copy your static UI in
   COPY ./my-app/ui /app/ui/
   
    # Install Python dependencies 
    WORKDIR /app
    RUN pip install -r requirements.txt
    ``` 

2. Build the Docker:

    ```bash
    docker build . -t my-image-name:latest 
    ```
   
3. Test it locally:
   
   Start the Docker first:
    ```bash
    docker run -p 80:80 my-image-name:latest
    ```
   
   Now fire a request, and see if all is well:
   
   `curl -v "http://localhost/"` 
   
   The default app returns a cheery "Hello, World!"

## Configuration

The configuration of Supervisord, nginx, and uvicorn can be extended or overwritten by putting files in the following structure: 

```text
    / (root)
    ├── app                             => website folder
    │   ├── main.py                     => Python module
    │   └── static                      => static files folder, served directly by Nginx
    │       ├── index.html
    │       └── static              
    │           └── logo.png
    └── etc
        ├── before-start.sh             => Bash file for additional startup logic
        ├── entrypoint.sh               => Standard Docker entrypoint; calls before-start.sh, 
        |                                   then starts supervisord.
        ├── nginx
        │   ├── nginx.conf              => Standard nginx configuration
        │   ├── sites-available
        │   │   └── default.template    => site-specific configuration; this template is used during startup 
        |   |                               to generate /etc/nginx/sites-enabled/default.           
        │   └── sites-enabled
        └── supervisor
            ├── conf.d
            │   ├── nginx.conf          => nginx startup configuration
            │   ├── uvicorn.conf        => uvicorn startup configuration 
            |   └── *.conf              => other Supervisor process configuration files (add if needed) 
            └── supervisord.conf        => Standard supervisord configuration

```

See the [`root`](https://gitlab.com/roemer/python-nginx-uvicorn/tree/master/root) directory.   

## Application 

The Python uvicorn app (e.g. FastAPI) should be placed in `/app/main.py`, and have a callable named `app`. 
Otherwise, provide a new `uvicorn.conf` file in `/etc/supervisor/conf.d` with the appropriate entrypoint.

## Supervisor
Supervisor is configured to start `nginx` and `uvicorn`. More processes can be started by placing additional `.conf` files in `/etc/supervisor/conf.d`.

This is the startup configuration for Uvicorn:

```ini
[program:uvicorn]
command=uvicorn --fd 0 main:app --host 0.0.0.0 --port 8080

stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0

stderr_logfile=/dev/stdout
stderr_logfile_maxbytes=0

stopsignail = QUIT
startsecs = 0
autostart=true
``` 

